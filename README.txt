Provides links for social networks (Facebook, Twitter, Orkut and Forward) to
share your content and tracks statistics by registered users.

There is also a report where you can see a table with user's name, the node
title, the service name and the date when the user shared your content.

By default this module shortens the url using a call to TinyUrl service. If you
don't want it, you can remove this at settings configuration page.

CASE:
You want to track your registered users if they are sharing content by social
network.

USAGE
- add block Share Stats to your theme region
- or simply copy and paste the call below to your node.tpl where you'd like to
  have your social buttons:
<?php
print share_stats_render($nid) ;
?>

EXTENDING
In the future this module will have hook for add new services and a gui for
them. But by now, you need to edit share_stats.module to add a new social
network at function share_stats_click on switch ($service). Also add your new
service at function share_stats_link_check. It's simple, just look the code.

LIMITATIONS:
This module monitors the click, you won't have guaranty if the user concluded
the sharing process. By now you'll need to manually edit the code to add new
services, but at the future this module can grow and have a UI configuration to
add more services.

RECOMMENDATIONS:
If you don't need users statistics, look for Service Links module. If you don't
need to know which user clicked where, you should track by Google Analytics.
